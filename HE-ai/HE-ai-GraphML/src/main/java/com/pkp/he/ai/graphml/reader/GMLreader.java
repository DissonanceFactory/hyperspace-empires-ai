package com.pkp.he.ai.graphml.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.impl.util.FileUtils;
import org.neo4j.tooling.GlobalGraphOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.util.io.graphml.GraphMLReader;

public class GMLreader {
	Logger logger = LoggerFactory.getLogger(GMLreader.class);

	static GraphDatabaseService graphDb;
	static Properties props;
	static InputStream input;

	private static enum RelTypes implements RelationshipType {
		TO
	}

	public static void main(String[] args) {

		readProperties(args[0]);
		clearDb();
		readXML();
		duplicateEdges();
		System.exit(0);
	}

	public static void readProperties(String propsPath) {
		props = new Properties();

		try {

			input = new FileInputStream(propsPath);

			// load a properties file
			props.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void testDb() {
		graphDb = new GraphDatabaseFactory()
				.newEmbeddedDatabaseBuilder(
						props.getProperty("neo4j.db.location"))
				.loadPropertiesFromFile(
						"src/main/resources/config/EmbeddedNeoDBConfig")
				.newGraphDatabase();
		System.out.println("Graph connected OK");

		registerShutdownHook(graphDb);
		graphDb.shutdown();
	}

	public static void readXML() {
		// clearDb();
		Graph graph = null;
		System.out.println("check");
		dumpProps();
		String inputGraph = props.getProperty("graphml.folder")
				+ props.getProperty("graphml.input.file");
		try {
			graph = new Neo4jGraph(props.getProperty("neo4j.db.location"));
			input = new FileInputStream(inputGraph);
			GraphMLReader reader = new GraphMLReader(graph);
			reader.inputGraph(input);
			input.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Graph read OK");
		graph.shutdown();
	}

	
	/**
	 * Traverse the nodes in the graph.
	 * Take all the _default edges out of a node and do the following
	 * 		Create a new edge out of this node, relationship type TO
	 * 		Create a new edge into this node, relationship type TO
	 * 		Delete the original edge, relationship type _default
	 * 
	 * Now we have new directed edges in and out of the node.
	 */
	private static void duplicateEdges() {
		System.out.println("Modifying edges");
		//TODO un hardcode this shit
		graphDb = new GraphDatabaseFactory()
				.newEmbeddedDatabaseBuilder(
						props.getProperty("neo4j.db.location"))
				.loadPropertiesFromFile(
						"E:/Git/hyperspace-empires-ai/HE-ai/configs/Neo.config")
				.newGraphDatabase();
		Relationship relationship;
		Node target;
		for (Node n : GlobalGraphOperations.at(graphDb).getAllNodes()) {
			System.out.println(n.getId());
			Transaction tx = graphDb.beginTx();
				for (Relationship r : n.getRelationships(Direction.OUTGOING)) {
					if(!r.isType(RelTypes.TO)){
						System.out.println(r);
						target = r.getEndNode();
						relationship = n.createRelationshipTo(target, RelTypes.TO);
						System.out.println(Arrays.toString(relationship.getNodes()));
						relationship = target.createRelationshipTo(n, RelTypes.TO);
						System.out.println(Arrays.toString(relationship.getNodes()));
						r.delete();
					}

				}
			tx.success();
			tx.finish();
		}
		graphDb.shutdown();
		System.out.println("OK");
	}

	private static void dumpProps() {
		for (Object k : props.keySet()) {
			System.out.printf("%s %s\n", k, props.get(k));
		}
	}

	private static void clearDb() {
		try {
			FileUtils.deleteRecursively(new File(props
					.getProperty("neo4j.db.location")));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	void shutDown() {
		System.out.println();
		System.out.println("Shutting down database ...");
		// START SNIPPET: shutdownServer
		graphDb.shutdown();
		// END SNIPPET: shutdownServer
	}

	// START SNIPPET: shutdownHook
	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running application).
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}
	// END SNIPPET: shutdownHook
}

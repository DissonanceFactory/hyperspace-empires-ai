package com.pkp.he.ai.smarts.combatEvaluation;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pkp.he.ai.smarts.structure.StructureEvaluator;

/**
 * This is a temporary class, where we fake the information that the game gives
 * to the player relating to combat.
 * 
 * It is important that we use an interface to the program here, as we may
 * change it at some point
 * 
 * @author aj The probable outcome is based on running the actual combat round
 *         10,000 times and returning the average result. I have included the
 *         combat function below. its very simple so you should be able to grok
 *         it. The AI could call the function to get its combat probability. The
 *         code is on github in Assets/Code/Gameplay/Combat.cs
 *         
 *         It looks like the mechanic can be summed up as follows. I'm going for plain english to ensure the AI isn't cheating with it's evaluation of an acceptable risk.

    Each attacking ship has an attack.
    rolls 1-10 + modifiers vs defender roll of 1-10 + modifiers
    Loser loses a ship.
    If this lost ship is the attacker, it may have already attacked else it loses its attack.

Currently the only modifier is force of numbers, which adds a +1 for every 20 ships difference.

 * 
 */
public class CombatPredictor {
	Logger logger = LoggerFactory.getLogger(CombatPredictor.class);

	 Random randomGenerator = new Random();
	 
	 int attackForceSize,defenseForceSize;
	 float forceOfNumbersBonus=0.05f;
	 float baseAttack =0;
	 float attackBonuses=0;
	 float baseDefense=0; 
	 float defenseBonuses=0;
	double [] attackRandomVariation = {1,10};
	double [] defenseRandomVariation = {1,10};
	 
	 
	 boolean defenderDamageFlag = true;
	 boolean attackerDamageFlag = true;
	 public int samplesForAverageOutcome =10000;
	 
	public float[] AverageOutcome(int attackForceSize,int defenseForceSize)
	    {
		this.attackForceSize = attackForceSize;
		this.defenseForceSize = defenseForceSize;

		
	        float[] acc = new float[2];

	        for (int i = 0; i < samplesForAverageOutcome; i++)
	        {
	            float[] result = Calculate();

	            acc[0] += result[0];
	            acc[1] += result[1];
	        }
	        return new float[] { acc[0] / (float)samplesForAverageOutcome, acc[1] / (float)samplesForAverageOutcome };
	    }
	    
	// float[0] = attackers remaining, float[1] defenders remaining
	public float[] Calculate() {
		int defenders = defenseForceSize;
		int attackers = attackForceSize;
		int attackIndex = 0;


		while ((attackIndex < attackForceSize) && (defenders > 0)) {
			float afonb = forceOfNumbersBonus * attackers;
			float dfonb = forceOfNumbersBonus * defenders;
			float att = baseAttack + attackBonuses + afonb;
			float def = baseDefense + defenseBonuses + dfonb;

			float attRoll = (float) (att + ((Math.random()*attackRandomVariation[1])+attackRandomVariation[0]));
/*					+ Random.Range(attackRandomVariation[0],
							attackRandomVariation[1]);*/
			float defRoll = (float) (att + ((Math.random()*defenseRandomVariation[1])+defenseRandomVariation[0]));
/*			float defRoll = def
					+ Random.Range(defenseRandomVariation[0],
							defenseRandomVariation[1]);*/

			if (attRoll > defRoll) {
				if (defenderDamageFlag) {
					defenders--;
				}
			} else if (attRoll < defRoll) {
				if (attackerDamageFlag) {
					attackers--;
					// check to see if the attacker destroyed was still to
					// attack and if so reduce the attack index by 1
					if ((Math.random() *attackForceSize) > attackIndex) {
						attackIndex++;
					}
				}
			}
			attackIndex++;
		}
		return new float[] { attackers, defenders };
	}
}

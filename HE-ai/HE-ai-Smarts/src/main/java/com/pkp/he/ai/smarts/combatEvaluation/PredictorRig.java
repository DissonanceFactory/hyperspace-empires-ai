package com.pkp.he.ai.smarts.combatEvaluation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PredictorRig {
	Logger logger = LoggerFactory.getLogger(PredictorRig.class);

	public static void main(String[] args) {
		int attackers = 500;
		int defenders = 200;
		CombatPredictor c = new CombatPredictor();
		float rslt[] = c.AverageOutcome(attackers, defenders);
		System.out.printf("Attackers %d Defenders %d\n"+
				"Remain %f %f", attackers, defenders, rslt[0], rslt[1]);

	}

}

package com.pkp.he.ai.smarts.objectives;

import java.util.TreeSet;

public class LTO implements Objective, Comparable {
	//
	Double fitness;
	TreeSet<STO> objectivesList;
	int completion;

	public LTO(){
		
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setFitness(double fitness) {
		this.fitness = fitness;

	}

	@Override
	public double getFitness() {
		return fitness;
	}

	@Override
	public void setCompletion(int completion) {
		this.completion = completion;

	}

	@Override
	public int getCompletion() {
		return completion;
	}
}

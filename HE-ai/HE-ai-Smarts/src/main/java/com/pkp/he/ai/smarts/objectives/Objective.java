package com.pkp.he.ai.smarts.objectives;

/**
 * This is the interface for all objectives.
 * 
 * @author aj
 *
 */
public interface Objective {

	public void setFitness(double fitness);
	public double getFitness();
	
	public void setCompletion(int completion);
	public int getCompletion();
			
}

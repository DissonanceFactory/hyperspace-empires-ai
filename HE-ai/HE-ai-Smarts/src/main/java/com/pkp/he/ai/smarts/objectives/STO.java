package com.pkp.he.ai.smarts.objectives;

import org.neo4j.graphdb.Node;

/**
 *  Diffuse from : This is a distribution point in some safe area which is defended at some other key location
    Garrison : Leave a minimal garrison at this node.
    Reserves : Station a reserve force at a location. This is to allow deployment in different areas from some well connected node.
    Deploy to : Move to the node in force. This will be a point AI wishes to defend.
    Mobilise to : Move to the node in force with the intention of deploying from there.
 * @author aj
 *
 */
public class STO implements Objective, Comparable {

	Node node;
	Double fitness;
	int completion;
	/**
	 * Node target - the objective node
	 * Type of objective defined as above
	 * Some kind of fitness measure
	 */
	
	public STO(){
		
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void setFitness(double fitness) {
		this.fitness=fitness;
		
	}
	@Override
	public double getFitness() {
		return fitness;
	}
	@Override
	public void setCompletion(int completion) {
		this.completion=completion;
		
	}
	@Override
	public int getCompletion() {
		return completion;
	}
}

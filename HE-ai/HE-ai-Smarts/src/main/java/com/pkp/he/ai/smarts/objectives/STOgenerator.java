package com.pkp.he.ai.smarts.objectives;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import com.pkp.he.ai.neo.NodeComparator;

public class STOgenerator {
	TreeSet<Node> nodeList;
	
	public STOgenerator(){
	
	}

	public SortedSet<Node> rankNodes(GraphDatabaseService dataBase){
		
		Comparator comparator = new NodeComparator();
	
		SortedSet<Node> nodes = new TreeSet<Node>(comparator);
		try (Transaction tx = dataBase.beginTx()) {
			for (Node n : dataBase.getAllNodes()) {
				nodes.add(n);
			}
			tx.success();
		}	
		return nodes;
	}
	
	public String outputNodeRank(){
		return null;
		
	}
}

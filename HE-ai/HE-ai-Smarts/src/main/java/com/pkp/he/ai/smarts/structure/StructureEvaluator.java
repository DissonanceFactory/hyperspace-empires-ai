package com.pkp.he.ai.smarts.structure;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.impl.util.FileUtils;
import org.neo4j.tooling.GlobalGraphOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StructureEvaluator {
	Logger logger = LoggerFactory.getLogger(StructureEvaluator.class);

	static GraphDatabaseService graphDb;
	static Properties props;
	static InputStream input;
	static double alpha;
	static double beta;
	static double delta;
	static double gamma;
	static int MAXDEPTH;
	static double weight;
	static double degree;
	static double diffW;
	static double diffD;
	
	public StructureEvaluator(String props){
		readProperties(props);
		openDb();
	}
	
	/**
	 * Bridge edge if each of its end nodes is either a cut node or a pendant node (ie degree one)
	 * 
	 * Cut node iff subgraph induced by removing the node is disconnected.
	 * There are many cut nodes - most of them. What is the cut value of them?
	 * 		Each disjoint subgraph could be valued by the sum of the nodes (what metrics here?)
	 * 		Maybe apply the value to the edge; so that the cut value is relative to where the player is travelling from or is concentrated.
	 */

	public void shutdown(){
		graphDb.shutdown();
	}
	
	public static void setDegrees() {
		// ArrayList<Node> nodes = new ArrayList<>();
		try (Transaction tx = graphDb.beginTx()) {
			for (Node node : GlobalGraphOperations.at(graphDb).getAllNodes()) {
				if (!node.hasProperty("nodeDegree")) {
					node.setProperty("nodeDegree", getDegree(node.getId()));
				}
			}
			tx.success();
		}
	}

	public static void removeLabels() {
		try (Transaction tx = graphDb.beginTx()) {
			for (Node node : GlobalGraphOperations.at(graphDb).getAllNodes()) {
				for (org.neo4j.graphdb.Label l : node.getLabels()) {
					node.removeLabel(l);
				}
			}
			tx.success();
		}
	}

	public static void evaluateNodes() {
		// ArrayList<Node> nodes = new ArrayList<>();
		try (Transaction tx = graphDb.beginTx()) {
			for (Node node : GlobalGraphOperations.at(graphDb).getAllNodes()) {
				long id = node.getId();
				System.out.println("-----" + id + "------");
				System.out.println("This weight: "
						+ node.getProperty("nodeWeight"));
				System.out.println("This degree: "
						+ node.getProperty("nodeDegree"));

				HashMap<Integer, Integer> weights = getDiffusedValue(
						"nodeWeight", id, MAXDEPTH);
				HashMap<Integer, Integer> degrees = getDiffusedValue(
						"nodeDegree", id, MAXDEPTH);
				double diffW = formula(weights);
				double diffD = formula(degrees);
				System.out.println("weights: " + diffW);
				System.out.println("degrees: " + diffD);
				if (diffW == 0)
					diffW = -1;
				if (diffD == 0)
					diffD = -1;
				node.setProperty("diffWeight", diffW);
				node.setProperty("diffDegree", diffD);
			}
			tx.success();
		}
	}
	
	public static void evaluateCuts() {
		// ArrayList<Node> nodes = new ArrayList<>();
		try (Transaction tx = graphDb.beginTx()) {
			for (Relationship edge : GlobalGraphOperations.at(graphDb).getAllRelationships()) {
				Node n1= edge.getStartNode();
				Node n2 = edge.getEndNode();
				//THis is backwards feeling but returns intuitively --check
				double d1 = calculateSubgraph(n2, n1);
				edge.setProperty("value", d1);
				System.out.printf("Edge %s N %d -%d  val %f\n", edge.getId(), n1.getId(),n2.getId(), d1);
			}
			tx.success();
		}
	}
	
	private static double calculateSubgraph(Node node, Node stop){
		Double value=0.0;
		Traverser subgraph = breadthFirstSearch(node);
		Set<Node> nodes = new HashSet<Node>();
		for (Path path : subgraph) {
			for(Node n : path.nodes()){
				if(n.equals(stop)){
					break;
				}
				nodes.add(n);
			}
		}
		
	    for (Iterator<Node> it = nodes.iterator(); it.hasNext(); ) {
	        Node n = it.next();
	        double v = Double.parseDouble((String) n.getProperty("value"));
	        value+=v;
	    }
	    
		return value;
	}

	public static void rankNodes() {
		try (Transaction tx = graphDb.beginTx()) {
			for (Node node : GlobalGraphOperations.at(graphDb).getAllNodes()) {
				double rank = rank((int) node.getProperty("nodeWeight"),
						(int) node.getProperty("nodeDegree"),
						(double) node.getProperty("diffWeight"),
						(double) node.getProperty("diffDegree"));
				node.addLabel( DynamicLabel.label(""+node.getProperty("nodeWeight")));
				//node.setProperty("value", df.format(rank));
				DecimalFormat df = new DecimalFormat();
				df.setMaximumFractionDigits(2);
				node.setProperty("value",df.format(rank));
			}
			tx.success();
		}
	}

	private static int getDegree(long id) {
		int degree = 0;
		try (Transaction tx = graphDb.beginTx()) {
			Node thisNode = graphDb.getNodeById(id);
			System.out.println("Node " + id);
			for (Relationship r : thisNode.getRelationships(Direction.OUTGOING)) {
				degree++;
			}
			System.out.println("\tDegree " + degree);

			tx.success();
		}
		return degree;
	}

	/**
	 * Potentially redundant wrapper function
	 * 
	 * @param id
	 * @return
	 */
	private static int getWeight(long id) {
		int weight = 0;
		try (Transaction tx = graphDb.beginTx()) {
			Node thisNode = graphDb.getNodeById(id);
			System.out.println("Node " + id);
			weight = (Integer) thisNode.getProperty("nodeWeight");
			System.out.println("\tweight " + weight);
			tx.success();
		}
		return weight;
	}

	private static HashMap<Integer, Integer> getDiffusedValue(String key,
			long id, int depth) {

		HashMap<Integer, Integer> diff = new HashMap<Integer, Integer>();
		try (Transaction tx = graphDb.beginTx()) {
			Node neoNode = graphDb.getNodeById(id);
			ArrayList<Long> visited = new ArrayList<Long>();

			Traverser diffuser = breadthFirstSearch(neoNode);

			for (Path diffusedPath : diffuser) {
				int length = diffusedPath.length();
				if (length == depth)
					break;
				int accumulator = 0;
				
				long targetNodeID;
				for (Node pathNode : diffusedPath.nodes()) {
					targetNodeID = pathNode.getId();
					if (!visited.contains(targetNodeID)&&targetNodeID!=id) {
						accumulator += ((Integer) pathNode.getProperty(key));
						diff.put(length, accumulator);
						visited.add(targetNodeID);
/*						System.out.println("\nAdding "+targetNodeID);
						for (Long ll : visited) {
							System.out.print(ll + " .. ");
						}*/

					}
				}

			}
			tx.success();
		}
		return diff;
	}

	private static Traverser breadthFirstSearch(final Node nd) {
		@SuppressWarnings("deprecation")
		TraversalDescription td = Traversal.description().breadthFirst()
				.relationships(RelTypes.TO, Direction.OUTGOING)
				.evaluator(Evaluators.excludeStartPosition());
		return td.traverse(nd);
	}
	
	private static Traverser depthFirstSearch(final Node nd) {
		@SuppressWarnings("deprecation")
		TraversalDescription td = Traversal.description().depthFirst()
				.relationships(RelTypes.TO, Direction.OUTGOING)
				.evaluator(Evaluators.excludeStartPosition());
		return td.traverse(nd);
	}

	private static double formula(HashMap<Integer, Integer> map) {
		double retVal = 0.0;

		retVal = alpha * protectedGet(map, 1) + beta * protectedGet(map, 2)
				+ delta * protectedGet(map, 3) + gamma * protectedGet(map, 4);

		return retVal;
	}

	private static double rank(double w, double x, double y, double z) {
		double retVal = 0.0;

		retVal = weight * w + degree * x + diffW * y + diffD * z;

		return retVal;
	}

	private static int protectedGet(HashMap<Integer, Integer> map, int index) {
		if (map.containsKey(index)) {
			return map.get(index);
		}
		return 0;
	}

	public static void readProperties(String propsPath) {
		props = new Properties();

		try {

			input = new FileInputStream(propsPath);

			// load a properties file
			props.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// TODO use spring here
		alpha = Double.parseDouble((String) props.get("diffuse.alpha"));
		beta = Double.parseDouble((String) props.get("diffuse.beta"));
		delta = Double.parseDouble((String) props.get("diffuse.delta"));
		gamma = Double.parseDouble((String) props.get("diffuse.gamma"));
		MAXDEPTH = Integer.parseInt((String) props.get("diffuse.maxdepth"));
		weight = Double.parseDouble((String) props.get("rank.weight"));
		degree = Double.parseDouble((String) props.get("rank.degree"));
		diffW = Double.parseDouble((String) props.get("rank.diffW"));
		diffD = Double.parseDouble((String) props.get("rank.diffD"));
	}

	public static void openDb() {
		graphDb = new GraphDatabaseFactory()
				.newEmbeddedDatabaseBuilder(
						props.getProperty("neo4j.db.location"))
				.loadPropertiesFromFile(
						"E:/Git/hyperspace-empires-ai/HE-ai/configs/Neo.config")
				.newGraphDatabase();
		System.out.println("Graph connected OK");

		registerShutdownHook(graphDb);
	}

	private static void clearDb() {
		try {
			FileUtils.deleteRecursively(new File(props
					.getProperty("neo4j.db.location")));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	void shutDown() {
		System.out.println();
		System.out.println("Shutting down database ...");
		// START SNIPPET: shutdownServer
		graphDb.shutdown();
		// END SNIPPET: shutdownServer
	}

	// START SNIPPET: shutdownHook
	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running application).
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}

	// END SNIPPET: shutdownHook
}

package objectiveTests;

import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;

import com.pkp.he.ai.neo.DataBaseHandler;
import com.pkp.he.ai.smarts.objectives.STOgenerator;

public class TestSTOGenerator {
	
	@Test
	public void testRank(){
		String propsFile = "src/test/resources/HE.properties";
		DataBaseHandler db = new DataBaseHandler();
		db.readProperties(propsFile);
		db.openDb();
		System.out.println("test begins ...");
		GraphDatabaseService dataBase = db.getDB();
		STOgenerator stoGen = new STOgenerator();
		stoGen.rankNodes(dataBase);
		stoGen.outputNodeRank();
	}

}

package structureEvaluationTests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.pkp.he.ai.smarts.structure.StructureEvaluator;

public class TestStructureEvaluator {

	//Here we need to design some graph that displays an easy to evaluate structure.
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		String propsFile="src/test/resources/HE.properties";
		StructureEvaluator evaluator = new StructureEvaluator(propsFile);					
		evaluator.setDegrees();
		evaluator.removeLabels();
		evaluator.evaluateNodes();
		evaluator.rankNodes();
		evaluator.evaluateCuts();
		evaluator.shutdown();
	}

}

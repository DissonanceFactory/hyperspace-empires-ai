package com.pkp.he.ai.neo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.impl.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This will be a single object which handles the database.
 * It can be passed around between components as they extract data or perform calcs.
 * @author aj
 *
 */
public class DataBaseHandler {
	static GraphDatabaseService graphDb;
	static Properties props;
	static InputStream input;

	// Logger logger = LoggerFactory.getLogger(DataBaseHandler.class);

//	Logger logger =LoggerFactory.getLogger(DataBaseHandler.class);
	
	public DataBaseHandler() {

	}

	public void readProperties(String propsPath) {
		props = new Properties();

		try {

			input = new FileInputStream(propsPath);

			// load a properties file
			props.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void openDb() {
		graphDb = new GraphDatabaseFactory()
				.newEmbeddedDatabaseBuilder(
						props.getProperty("neo4j.db.location"))
				.loadPropertiesFromFile(
						"E:/Git/hyperspace-empires-ai/HE-ai/configs/Neo.config")
				.newGraphDatabase();
		System.out.println("Graph connected OK");
		registerShutdownHook(graphDb);
	}
	
	public GraphDatabaseService getDB(){
		return graphDb;
	}

	public void clearDb() {
		try {
			FileUtils.deleteRecursively(new File(props
					.getProperty("neo4j.db.location")));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void shutDown() {
		System.out.println();
		System.out.println("Shutting down database ...");
		// START SNIPPET: shutdownServer
		graphDb.shutdown();
		// END SNIPPET: shutdownServer
	}

	// START SNIPPET: shutdownHook
	private void registerShutdownHook(final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running application).
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}
}

package com.pkp.he.ai.neo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DelineateNode/* implements ApplicationContextAware */{
/*
	
	public DelineateNode(String owner, double strength, String nodeFunction,
			String membership) {
		super();
		this.owner = owner;
		this.strength = strength;
		this.nodeFunction = nodeFunction;
		this.membership = membership;
	}*/
	Logger logger = LoggerFactory.getLogger(DelineateNode.class);

	private String owner;
	
	private double strength;
	
	private String nodeFunction;
	
	private String membership;
	
	public double getStrength() {
		return strength;
	}

	public void setStrength(double strength) {
		this.strength = strength;
	}

	public String getNodeFunction() {
		return nodeFunction;
	}

	public void setNodeFunction(String nodeFunction) {
		this.nodeFunction = nodeFunction;
	}

	public String getMembership() {
		return membership;
	}

	public void setMembership(String membership) {
		this.membership = membership;
	}

	public String toString(){
		String retval="";
		retval=String.format("Owner %s,  Strength %d, nodeFunction %s, membership %s",  
				owner, strength, nodeFunction, membership );
		return retval;
	}

/*	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		// TODO Auto-generated method stub
		
	}*/
}

package com.pkp.he.ai.neo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.impl.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GraphConnector {
	Logger logger = LoggerFactory.getLogger(GraphConnector.class);

	static GraphDatabaseService graphDb;
	static Properties props;
	static InputStream input;

	public static void main(String[] args) {

		readProperties(args[0]);
		
	}

	public static void readProperties(String propsPath) {
		props = new Properties();

		try {

			input = new FileInputStream(propsPath);

			// load a properties file
			props.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void testDb() {
		graphDb = new GraphDatabaseFactory()
				.newEmbeddedDatabaseBuilder(
						props.getProperty("neo4j.db.location"))
				.loadPropertiesFromFile(
						"E:/Git/hyperspace-empires-ai/HE-ai/configs/Neo.config")
				.newGraphDatabase();
		System.out.println("Graph connected OK");

		registerShutdownHook(graphDb);
		graphDb.shutdown();
	}


	private static void clearDb() {
		try {
			FileUtils.deleteRecursively(new File(props
					.getProperty("neo4j.db.location")));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	void shutDown() {
		System.out.println();
		System.out.println("Shutting down database ...");
		// START SNIPPET: shutdownServer
		graphDb.shutdown();
		// END SNIPPET: shutdownServer
	}

	// START SNIPPET: shutdownHook
	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running application).
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}
	// END SNIPPET: shutdownHook
}

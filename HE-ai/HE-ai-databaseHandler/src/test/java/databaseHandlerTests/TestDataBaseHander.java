package databaseHandlerTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import com.pkp.he.ai.neo.DataBaseHandler;

public class TestDataBaseHander {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		String propsFile = "src/test/resources/HE.properties";
		DataBaseHandler db = new DataBaseHandler();
		db.readProperties(propsFile);
		db.openDb();
		System.out.println("test begins ...");
		GraphDatabaseService dataBase = db.getDB();
		try (Transaction tx = dataBase.beginTx()) {
			for (Node n : dataBase.getAllNodes()) {
				System.out.println(n.getId());
			}
			tx.success();
		}
	}

}

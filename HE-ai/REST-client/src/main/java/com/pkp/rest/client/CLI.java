package com.pkp.rest.client;

import java.util.ArrayList;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CLI {
	Logger logger = LoggerFactory.getLogger(CLI.class);

	public static void main(String[] args) {
		menu();
	}

	public static int menu() {
		// Displays the main menu and handles passing off to next menu.

		JerseyRestClient neo = new JerseyRestClient();
		Scanner scanner = new Scanner(System.in);
		int selection = 0;
		String id = "";
		String action = "";
		int i = 0;
		int j = 0;

		while (i == 0) { // changed while(1) to a value that didn't complain in
							// netbeans
			System.out.println("Please choose an option from the following:");
			System.out.println("[1] Get a node");
			System.out.println("[2] Get an edge");
			System.out.println("[3] GetAllNodes");
			System.out.println("[4] GetAllRels");
			System.out.println("[5] ShortestPath");
			System.out.println("[6] SpiderList");
			System.out.println("[9] Quit");
			System.out.println("Choice: ");
			selection = scanner.nextInt();

			switch (selection) {

			case 1:
				System.out.println("Please enter the Node to get:");
				id = scanner.next();
				action = "get node ";
				neo.getNode(id);
				break;

			case 2:
				System.out.println("Please enter the Edge to get:");
				id = scanner.next();
				action = "get edge ";
				neo.getEdge(id);
				break;

			case 3:
				System.out.println("----All Nodes ----");
				neo.getAllNodes();
				break;
			case 4:
				System.out.println("----All Rels ----");
				neo.getAllRels();
				break;
			case 5:
				System.out.println("----Shortest path ----");
				System.out.println("Enter source");
				String src = scanner.next();
				System.out.println("Enter sink");
				String dest = scanner.next();
				neo.shortestPath(src, dest);
				break;
			case 6:
				System.out.println("----Spider list ----");
				int spiderID =0;
				ArrayList<Integer> ids = new ArrayList<Integer>();
				while (spiderID != -1) {
					System.out.println("Enter next ID (-1 to exit)");
					String sid = scanner.next();
					spiderID = Integer.parseInt(sid);
					if(spiderID!=-1)ids.add(spiderID);
				}
				System.out.println("Enter depth");
				int depth = Integer.parseInt(scanner.next());
				for(int ii: ids){
					System.out.println(ii);
				}
				neo.spiderList(depth, ids);
				break;
			case 7:
				break;
			case 8:
				break;

			case 9:
				System.out.println("Quitting...");
				System.exit(5);

			default:
				System.out.println("Your choice was not valid!");

			}
			;

		}
		System.out.printf("%s %s\n", action, id);
		return selection;
	}
}

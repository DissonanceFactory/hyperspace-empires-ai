package com.pkp.rest.client;

import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class JerseyRestClient {
	Logger logger = LoggerFactory.getLogger(JerseyRestClient.class);

	Client client;
	
	public JerseyRestClient(){
		client = Client.create();
	}

	
	public void getNode(String node){
		try {
			WebResource webResource = client
					.resource("http://localhost:7474/db/data/node/"+node+"/properties");

			ClientResponse response = webResource.accept("application/json")
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}


			String output = response.getEntity(String.class);
			System.out.println("\n\nOutput from Server .... \n");
			System.out.println(output);
			System.out.println();

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	
	public void getEdge(String edge){
		try {
			WebResource webResource = client
					.resource("http://localhost:7474/db/data/relationships/"+edge+"/properties");

			ClientResponse response = webResource.accept("application/json")
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}


			String output = response.getEntity(String.class);
			System.out.println("\n\nOutput from Server .... \n");
			System.out.println(output);
			System.out.println();

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	
	public void getAllNodes(){
		try {
			//http://localhost:7474/db/data/ext/GetAll/graphdb/get_all_nodes
			WebResource webResource = client
					.resource("http://localhost:7474/db/data/ext/GetAll/graphdb/get_all_nodes");

			ClientResponse response = webResource.accept("application/json")
					.post(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}


			String output = response.getEntity(String.class);
			System.out.println("\n\nOutput from Server .... \n");
			System.out.println(output);
			System.out.println();

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	public void getAllRels(){
		try {
			//http://localhost:7474/db/data/ext/GetAll/graphdb/get_all_nodes
			WebResource webResource = client
					.resource("http://localhost:7474/db/data/ext/GetAll/graphdb/get_all_rels");

			ClientResponse response = webResource.accept("application/json")
					.post(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}


			String output = response.getEntity(String.class);
			System.out.println("\n\nOutput from Server .... \n");
			System.out.println(output);
			System.out.println();

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	
	public void shortestPath(String src, String dest){
		try {
			/*	To use it, just POST to this URL, with parameters as specified in the description and encoded as JSON data 
			 * content. F.ex for calling the shortest path extension (URI gotten from a GET to http://localhost:7474/db/data/node/123):
			curl -X POST http://localhost:7474/db/data/ext/GetAll/node/123/shortestPath \
			  -H "Content-Type: application/json" \
			  -d '{"target":"http://localhost:7474/db/data/node/456&depth=5"}'
			  */
			
			//Create parameters map
			  MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
			   queryParams.add("target", "http://localhost:7474/db/data/node/"+dest);
			
			   
			WebResource webResource = client
					.resource("http://localhost:7474/db/data/ext/GetAll/node/"+src+"/shortestPath").queryParams(queryParams);			

			ClientResponse response = webResource.accept("application/json")
					.post(ClientResponse.class);

			if (response.getStatus() != 200) {
				System.out.println(response.toString());
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
				
			}


			String output = response.getEntity(String.class);
			System.out.println("\n\nOutput from Server .... \n");
			System.out.println(output);
			System.out.println();

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	
	public void spiderList(int depth, List<Integer> ids){
		try {
			/*	To use it, just POST to this URL, with parameters as specified in the description and encoded as JSON data 
			 * content. F.ex for calling the shortest path extension (URI gotten from a GET to http://localhost:7474/db/data/node/123):
			curl -X POST http://localhost:7474/db/data/ext/GetAll/node/123/shortestPath \
			  -H "Content-Type: application/json" \
			  -d '{"target":"http://localhost:7474/db/data/node/456&depth=5"}'
			  */
			
			//Create parameters map
			  MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
			   queryParams.add("depth", ""+depth);
			   for(Integer i: ids){
				   queryParams.add("ids", ""+i);
			   }
			
			   
			WebResource webResource = client
					.resource("http://localhost:7474/db/data/ext/SpiderList").queryParams(queryParams);			

			ClientResponse response = webResource.accept("application/json")
					.post(ClientResponse.class);

			if (response.getStatus() != 200) {
				System.out.println(response.toString());
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
				
			}


			String output = response.getEntity(String.class);
			System.out.println("\n\nOutput from Server .... \n");
			System.out.println(output);
			System.out.println();

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}

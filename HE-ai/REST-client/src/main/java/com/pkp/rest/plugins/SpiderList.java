package com.pkp.rest.plugins;

/**
 * Licensed to Neo Technology under one or more contributor
 * license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright
 * ownership. Neo Technology licenses this file to you under
 * the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;
import org.neo4j.server.plugins.Description;
import org.neo4j.server.plugins.Name;
import org.neo4j.server.plugins.Parameter;
import org.neo4j.server.plugins.PluginTarget;
import org.neo4j.server.plugins.ServerPlugin;
import org.neo4j.server.plugins.Source;

// START SNIPPET: ShortestPath
public class SpiderList extends ServerPlugin {
	@Name("spiderList")
	@Description("Spider out from a list of ids.")
	@PluginTarget(GraphDatabaseService.class)
	public Set<Node> spiderList(
			@Source GraphDatabaseService graphDb,
			@Description("The number of steps to spider out.") @Parameter(name = "depth") String depth,
			@Description("The list of IDs") @Parameter(name = "ids") String[] ids) {

		List<Traverser> traversers = new ArrayList<Traverser>();

		// Create a list of traversers - one for each node in the list passed
		// in.
		for (String i : ids) {
			try (Transaction tx = graphDb.beginTx()) {
				Node node = graphDb.getNodeById(Integer.parseInt(i));
				traversers.add(breadthFirstSearch(node));
				tx.success();
			}
		}

		return calculateSubgraph(traversers, Integer.parseInt(depth));
	}

	private static Set<Node> calculateSubgraph(List<Traverser> traversers,
			int depth) {
		Set<Node> nodes = new HashSet<Node>();
		for (Traverser t : traversers) {
			for (Path path : t) {
				int d = 0;
				for (Node n : path.nodes()) {
					if (d == depth) {
						break;
					}
					nodes.add(n);
					d++;
				}
			}
		}
		return nodes;
	}

	private static Traverser breadthFirstSearch(final Node nd) {
		@SuppressWarnings("deprecation")
		TraversalDescription td = Traversal.description().breadthFirst()
				.evaluator(Evaluators.excludeStartPosition());
		return td.traverse(nd);
	}
}
// END SNIPPET: ShortestPath
